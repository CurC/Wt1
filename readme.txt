Auteurs: Curtis Chin Jen Sem 5601118
	 Menno Jansen 5677246
URL: 	 https://www.students.science.uu.nl/~5601118/p1/  OF
	 https://www.students.science.uu.nl/~5677246/
Getest op Chrome 48 en Firefox 44
Extra's: JQuery animaties bij het navigatie menu
	 nav.html die de navigatie houdt wordt bij elk bestand automatisch erbij geladen via JQuery
	 Referenties worden automatisch verbonden en gelinkt naar de bijbehorende bron zolang die in IEEE formaat zijn
	 Bij het gebruik va de Flickr API wordt de volgende resultaat van de zoekquery alvast gecachet
	 Bij de Flot grafiek in de Rails pagina wordt de dataset automatisch gegenereerd uit een cvs bestand
	 Achtergrond kleur wordt donkerder aan de hand van hoeveel er gescrolled is