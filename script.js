$(document).ready(function () {
    
    // Navigatie menu
    $("nav").load("nav.html", function () {
        
        // Navigatie menu animaties
        $("li.navhoofdkop").click(function () {
            $(this).siblings().children("ul").stop().slideUp('medium');
            $(this).next("li.navonderkop").children("ul").stop().slideToggle('medium');
        });
        
        $("nav").children("ul").mouseleave(function () {
            $(this).find("li.navonderkop").children().stop().slideUp('medium');
        });
        
        $("li.navhoofdkop").hover(function () {
            $(this).stop().animate({
                backgroundColor: '#363636'
            }, 200);
        }, function () {
            $(this).stop().animate({
                backgroundColor: '#242424'
            }, 200);
        });

        $("li.navhoofdkop2").hover(function () {
            $(this).stop().animate({
                backgroundColor: '#191919'
            }, 200);
        }, function () {
            $(this).stop().animate({
                backgroundColor: '#000000'
            }, 200);
        });
    });
    
    // Achtergrondkleur op basis van scroll
    var baseColor = $("body").css("background-color");
    $(window).scroll(function () {
        var rgb;
        rgb = baseColor.match(/\d+/g);
        console.log(JSON.stringify(rgb));
        for (var i in rgb) {
            rgb[i] = rgb[i] - Math.round(($(document).scrollTop() / ($(document).height() - $(window).height())) * (rgb[i] / 2));
        }
        console.log("#" + rgb[0].toString(16) + rgb[1].toString(16) + rgb[2].toString(16));
        $("body").animate().css("background", "#" + rgb[0].toString(16) + rgb[1].toString(16) + rgb[2].toString(16));
    });
    
    // Referenties
    function createReferentie() {
        
        // Maakt links van alle citaten in de bibliografie
        var num, url, beginpos, eindpos;
        $("p.bibliografie").find("a.citaat").each(function () {
            if ($(this).text().indexOf("Available") !== -1) {
                beginpos = $(this).text().indexOf("http");
                eindpos = $(this).text().indexOf("[Geopend") - 2;
                url = $(this).text().substring(beginpos, eindpos);
                $(this).attr("href", url);
            }
            beginpos = $(this).text().indexOf("[") + 1;
            eindpos = $(this).text().indexOf("]");
            num = $(this).text().substring(beginpos, eindpos);
            $(this).attr("id", num);
        });
        
        // Linkt elke verwijzing met zijn toebehorende citaat
        $("p:not(.bibliografie)").find("a.citaat").each(function () {
            num = $(this).text().match(/\d/);
            $(this).attr("href", window.location.pathname + "#" + num);
        });
    }
    createReferentie();
});
