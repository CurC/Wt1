$(document).ready(function () {
    
    // Flot grafiek met search statistiek van rails 
    $(".flot#searchgrafiek").each(function () {
        
        // Zet alle waarden uit het csv bestand in een array voo Flot
        var gData = [];
        $.get("assets/report.csv", function (data) {
            var dataSplit,
                entry,
                i;
            dataSplit = data.split("\n");
            for (i = 0; i < dataSplit.length; i += 1) {
                entry = dataSplit[i].split(",");
                gData.push([Date.parse(entry[0]), parseInt(entry[1], 10)]);
            }
            
            // De populariteit van Rails op Google Search grafiek
            var plot = $.plot($(".flot#searchgrafiek"), [gData], {
            
                yaxis: {
                    show: false,
                    max: 100
                },

                xaxis: {
                    mode: "time",
                    timeformat: "%m-'%y"
                },
                
                selection: {
                    mode: "x"
                }
            });
            
            // Een overzicht van de zoek grafiek
            var overzicht = $.plot($(".flot#searchoverzicht"), [gData], {
            
                yaxis: {
                    show: false,
                    max: 100
                },

                xaxis: {
                    mode: "time",
                    show: false
                },
                
                selection: {
                    mode: "x"
                }
            });
            
            // Verbindt de overzicht aan het werkelijke grafiek
            $("div.flot#searchgrafiek").bind("plotselected", function (event, ranges) {
                
                // Verandert de x-as waarden op basis van de geselecteerde periode
                plot.getAxes().xaxis.options.min = ranges.xaxis.from;
                plot.getAxes().xaxis.options.max = ranges.xaxis.to;
                plot.setupGrid();
                plot.draw();
                plot.clearSelection();
                overzicht.setSelection(ranges, true);
            });
            
            $("div.flot#searchoverzicht").bind("plotselected", function (event, ranges) {
                plot.setSelection(ranges);
            });
        });
    });

    
    // Flot grafiek met mousemove events per 0.1 seconden 
    $(".flot#mmgrafiek").each(function () {

        var data = [],
            tot = [],
            teller = 0;

        $(document).mousemove(function () {
            teller += 1;
        });

        // Vult de dataset die de grafiek gaat gebruiken
        function getGData() {

            data.shift();

            var vorige,
                tot = [],
                i;

            while (data.length < 25) {

                data.push(teller);

            }

            for (i = 0; i < data.length; i += 1) {
                tot.push([i, data[i]]);
            }
            teller = 0;
            return tot;
        }

        var plot = $.plot(".flot#mmgrafiek", [getGData()], {

            series: {
                shadowSize: 1
            },

            yaxis: {
                min: 0,
                max: 15
            },

            xaxis: {
                show: false
            }
        });

        function update() {
            plot.setData([getGData()]);
            plot.draw();
            setTimeout(update, 100);
        }

        update();
        
    });
});