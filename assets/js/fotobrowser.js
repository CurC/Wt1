$(document).ready(function () {
    
    // Foto browser
    // Vraagt eerst resultaten op van FLickr gebasseerd op een tag
    // Deze wordt dan in url formaat in een array gestopt, om deze dan
    // later op te halen
    var imageArr = [],
        i = 0,
        showImage = false;
    
    $("#f-searchb").click(function () {
        
        var tag = $("#f-searcht").val();
        imageArr = [];
        i = 0;
        
        if (!showImage) {
            $("#f-imgcontainer").append("<img id='f-img' alt='Flickr zoek resultaat'>");
            showImage = true;
        }
        
        // Pakt de zoekresultaten op van Flickr en slaat deze in url formaat op in een array
        $.get('https://api.flickr.com/services/rest/? &method=flickr.photos.search&tags=' + tag + '& api_key=6a68ad339841f6f464f9d3a8ef645bb4&per_page=75&sort=interestingness-desc', function (data) {
            $(data).find("photo").each(function (i, value) {
                var id = $(value).attr("id"),
                    secret = $(value).attr("secret"),
                    farm = $(value).attr("farm"),
                    server = $(value).attr("server");
                console.log("src", "https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg");
                imageArr.push("https://farm" + farm + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg");
            });
            cacheImage(imageArr[1]);
            $("#f-img").attr("src", imageArr[0]);
            $("#f-img").css("width", "auto");
        });
    });
    
    // Zet een foto in de browsercache
    function cacheImage(image) {
         var imgCache = new Image();
        imgCache.src = image;
    }
    
    // Fotobrowser controllers opmaak
    $("button#f-right").button({
        icons: { secondary:  "ui-icon-triangle-1-e"},
        label: "Volgende"
    });
    
    $("button#f-left").button({
        icons: { primary:  "ui-icon-triangle-1-w"},
        label: "Vorige"
    });
    
    // Vorige knop pakt de vorige foto in de array
    $("button#f-left").button().click(function () {
        if (i > 0) {
            i -= 1;
            
            if (i > 0) {
                cacheImage(imageArr[i - 1]);
            }
        }
        $("#f-img").attr("src", imageArr[i]);
    });
    
    // Volgende knop pakt de volgende foto in de array
    $("button#f-right").button().click(function () {
        if (i < imageArr.length - 1) {
            i += 1;
            
            if (i < imageArr.length - 1) {
                cacheImage(imageArr[i + 1]);
            }
        }
        $("#f-img").attr("src", imageArr[i]);
    });
});
